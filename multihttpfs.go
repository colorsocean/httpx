package httpx

import "net/http"

type multiHTTPFS struct {
	fss []http.FileSystem
}

func (this multiHTTPFS) Open(name string) (f http.File, err error) {
	for _, fs := range this.fss {
		f, err = fs.Open(name)
		if err == nil {
			break
		}
	}
	return
}

func MultiHTTPFS(fss ...http.FileSystem) http.FileSystem {
	return multiHTTPFS{fss}
}
