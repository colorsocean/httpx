package httpx

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"net/url"
)

const (
	headerContentType   = "Content-Type"
	headerContentLength = "Content-Length"

	headerContentTypeTextHtml        = "text/html; encoding=utf-8"
	headerContentTypeApplicationJson = "application/json; charset=utf-8"
	headerContentTypeTextJavascript  = "text/javascript; charset=utf-8"
)

var (
	ErrOnlyAutomaticResponse  = errors.New("Please, don't write anything to ResponseWriter directly")
	ErrCanNotSerializePayload = errors.New("Can not serialize payload")
	ErrAlreadySent            = errors.New("Response already sent")
)

/****************************************************************
** Common Helpers
********/

func URLEncode(path string) string {
	return (&url.URL{Path: path}).String()
}

func RequestBasicAuth(w http.ResponseWriter, realm string) {
	w.Header().Set("WWW-Authenticate", fmt.Sprintf(`Basic realm=%q`, realm))
}

// fixme: Not working at all
func ListenAndServeUnix(sock string, handler http.Handler) error {
	l, err := net.Listen("unix", sock)
	if err != nil {
		return err
	} else {
		err := http.Serve(l, handler)
		if err != nil {
			return err
		}
	}

	return nil
}

func HTMLPrintf(w http.ResponseWriter, code int, format string, a ...interface{}) error {
	w.Header().Set(headerContentType, headerContentTypeTextHtml)
	w.WriteHeader(code)
	_, err := fmt.Fprintf(w, format, a...)
	return err
}

func WriteJsonBody(w http.ResponseWriter, v interface{}, status int) error {
	data, err := json.MarshalIndent(v, "", "  ")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return err
	}

	w.Header().Set(headerContentType, headerContentTypeApplicationJson)
	w.Header().Set(headerContentLength, fmt.Sprint(len(data)))
	w.WriteHeader(status)
	_, err = w.Write(data)
	if err == nil {
		return err
	}

	return nil
}

func ReadJsonBody(r *http.Request, vout interface{}) error {
	body, err := ioutil.ReadAll(r.Body)

	if err != nil {
		return err
	}

	err = json.Unmarshal(body, vout)

	if err != nil {
		return err
	}

	return nil
}

/****************************************************************
** Kawaii HTTP Responses
********/

func kawaii(w http.ResponseWriter, status int, smiley, heading, format string, args ...interface{}) error {
	kawaii_tpl := `<pre><h1>%s %d %s</h1><br/>%s<br/><br/>%s<br/><br/></pre>`
	return HTMLPrintf(w, status, kawaii_tpl, smiley, status, smiley, heading, fmt.Sprintf(format, args...))
}

func Kawaii500(w http.ResponseWriter, format string, args ...interface{}) error {
	return kawaii(w, http.StatusInternalServerError, "(_O_)", "Internal Server Error!", format, args...)
}

func Kawaii404(w http.ResponseWriter, format string, args ...interface{}) error {
	return kawaii(w, http.StatusNotFound, "=(^_^)=", "Not Found!", format, args...)
}

func Kawaii403(w http.ResponseWriter, format string, args ...interface{}) error {
	return kawaii(w, http.StatusForbidden, "(_X_)", "Forbidden!", format, args...)
}

func Kawaii401(w http.ResponseWriter, format string, args ...interface{}) error {
	return kawaii(w, http.StatusUnauthorized, "(_x_)", "Not Authorized!", format, args...)
}
