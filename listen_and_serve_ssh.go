package httpx

import (
	"net/http"

	"golang.org/x/crypto/ssh"
)

//func Listen() _listen {

//}

//type Listener interface {
//	net.Listener
//	Serve()
//}

//type listenAndServe struct {
//}

type ListenAndServeSSHOptions struct {
	ssh.ClientConfig

	RemoteServerAddr string
	ListenAddr       string

	User       string
	Password   string
	PrivateKey string
}

func ListenAndServeSSH(config ListenAndServeSSHOptions, h http.Handler) error {
	if config.User != "" {
		config.ClientConfig.User = config.User
	}

	if config.User != "" && config.Password != "" {
		config.ClientConfig.Auth = append(config.ClientConfig.Auth, ssh.Password(config.Password))
	}

	if config.PrivateKey != "" {
		private, err := ssh.ParsePrivateKey([]byte(config.PrivateKey))
		if err != nil {
			return err
		}
		config.ClientConfig.Auth = append(config.ClientConfig.Auth, ssh.PublicKeys(private))
	}

	conn, err := ssh.Dial("tcp", config.RemoteServerAddr, &config.ClientConfig)
	if err != nil {
		return err
	}
	defer conn.Close()

	l, err := conn.Listen("tcp", config.ListenAddr)
	if err != nil {
		return err
	}
	defer l.Close()

	return http.Serve(l, h)
}
