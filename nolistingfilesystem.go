package httpx

import (
	"net/http"
	"os"
)

// Todo: Test performance / compare with standard `http.FileServer`

type NoListingFileSystem struct {
	FS           http.FileSystem
	Listing      bool
	ListingCheck func() bool
}

func (this NoListingFileSystem) Open(name string) (http.File, error) {
	listing := this.Listing
	if this.ListingCheck != nil {
		listing = this.ListingCheck()
	}

	if listing {
		return this.FS.Open(name)
	} else {
		f, err := this.FS.Open(name)
		if err != nil {
			return nil, err
		}
		return noReaddirFile{f}, nil
	}
}

type noReaddirFile struct {
	http.File
}

func (f noReaddirFile) Readdir(count int) ([]os.FileInfo, error) {
	return nil, nil
}

func NoListingFileServer(root http.FileSystem) http.Handler {
	return http.FileServer(&NoListingFileSystem{root, false, nil})
}
