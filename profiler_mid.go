package httpx

import (
	"fmt"
	"net/http"
	"sync"
	"time"

	"github.com/zenazn/goji/web"
)

func ProfilerGoji(tag string) func(c *web.C, h http.Handler) http.Handler {
	return func(c *web.C, h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			whook := &profilerResponseWriterHook{startTime: time.Now(), tag: tag, w: w}
			h.ServeHTTP(whook, r)
		})
	}
}

type profilerResponseWriterHook struct {
	startTime time.Time
	tag       string
	w         http.ResponseWriter
	once      sync.Once
}

func (this *profilerResponseWriterHook) ensureSet() {
	this.once.Do(func() {
		this.w.Header().Set("X-Debug-Response-Time-"+this.tag, fmt.Sprint(time.Now().Sub(this.startTime).Nanoseconds()))
	})
}

func (this *profilerResponseWriterHook) Header() http.Header {
	return this.w.Header()
}

func (this *profilerResponseWriterHook) Write(b []byte) (int, error) {
	this.ensureSet()
	return this.w.Write(b)
}

func (this *profilerResponseWriterHook) WriteHeader(s int) {
	this.ensureSet()
	this.w.WriteHeader(s)
}
